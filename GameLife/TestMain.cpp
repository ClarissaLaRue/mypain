#include <gtest/gtest.h>
#include "Parser.h"
#include "LifeParser.h"
#include "Domain.h"

using std::vector;

//--------------------------------Работающие тесты
TEST(TestsForParser, SetRules){
    Rules Rule;
    Rule.addOpt("--iterations", "-ic", 1, INT);
    Rule.addOpt("--field", "-f", 2, INT);
    Rule.addOpt("--output", "-o", 1, STRING);
    EXPECT_EQ(Rule.getArgName(0), "LongForm --iterations ShortForm -ic");
    EXPECT_EQ(Rule.getArgName(1), "LongForm --field ShortForm -f");
    EXPECT_EQ(Rule.getArgName(2), "LongForm --output ShortForm -o");
    EXPECT_EQ(Rule.getArgCount(0), "1");
    EXPECT_EQ(Rule.getArgCount(1), "2");
    EXPECT_EQ(Rule.getArgCount(2), "1");
    EXPECT_EQ(Rule.getArgType(0), "INT");
    EXPECT_EQ(Rule.getArgType(1), "INT");
    EXPECT_EQ(Rule.getArgType(2), "STRING");
}

TEST(TestsForParser, SetAllRules){
    Rules Rule;
    addAllOptions(Rule);
    EXPECT_EQ(Rule.getSize(), 8);
    EXPECT_EQ(Rule.isOpt("-f"), 1);
    EXPECT_EQ(Rule.isOpt("Net"), 0);
    EXPECT_EQ(Rule.getArgCount("-f"), 2);
    EXPECT_EQ(Rule.getArgType("-f"), INT);
}

TEST(TestsForParser, ParseOneOpt){
    Rules Rule;
    addAllOptions(Rule);
    vector<string> Args = {"-ic", "20", "--field", "10", "10", "--output" , "OutputFile",
                           "-k", "NoOpt"};
    Variant Game(Rule);
    Game.ParseOption(Args, 0);
    EXPECT_EQ(Game.getSizeROpt(), 1);
    EXPECT_EQ(Game.getRName(0), "-ic");
    EXPECT_EQ(Game.getRArg("-ic"), "20");

    Game.ParseOption(Args, 2);
    EXPECT_EQ(Game.getSizeROpt(), 2);
    EXPECT_EQ(Game.getRName(1), "--field");
    EXPECT_EQ(Game.getRArg("--field"), "10 10");

    Game.ParseOption(Args, 5);
    EXPECT_EQ(Game.getSizeROpt(), 3);
    EXPECT_EQ(Game.getRName(2), "--output");
    EXPECT_EQ(Game.getRArg("--output"), "OutputFile");
}

TEST(TestsForParser, TryParse){
    Rules Rule;
    addAllOptions(Rule);
    vector<string> Args = {"-ic", "20", "--field", "10", "10",
                           "-m", "3", "-n", "2", "-k", "1"};

    Variant Game;
    Game = Parse(Rule, 11, Args);
    EXPECT_EQ(Game.getSizeROpt(), 5);
}

//--------------------------------В процессе

TEST(TestForDomain, TestForFieldToFindLifeN){
    Field field(5, 5);
    field.setLife(0, 0);
    field.setLife(2, 0);
    field.setLife(4, 1);
    field.setLife(1, 2);
    field.setLife(2, 3);
    field.setLife(1, 4);
    field.setLife(3, 4);

    field.setDead(2, 0);
    EXPECT_EQ(field.isLife(2, 0), DEAD);
    EXPECT_EQ(field.lifeNeighbors(2, 3), 3);
    EXPECT_EQ(field.lifeNeighbors(0, 0), 2);
    EXPECT_EQ(field.lifeNeighbors(2, 0), 2);
}

TEST(TestForDomain, TestOneIter){
    Configuration Config (1, 3, 2, 3, 5, 5);
    Game Life(Config);

    Field field(5, 5);
    //*.*..
    //....*
    //.*...
    //..*..
    //.*.*.
    field.setLife(0, 0);
    field.setLife(2, 0);
    field.setLife(4, 1);
    field.setLife(1, 2);
    field.setLife(2, 3);
    field.setLife(1, 4);
    field.setLife(3, 4);

    Life.setField(field);
    Life.OneIteration();
    //*****
    //**...
    //.....
    //.**..
    //.*.*.
    EXPECT_EQ(Life.isLife(0, 0), LIFE);
    EXPECT_EQ(Life.isLife(2, 0), LIFE);
    EXPECT_EQ(Life.isLife(4, 1), DEAD);
    EXPECT_EQ(Life.isLife(1, 2), DEAD);
    EXPECT_EQ(Life.isLife(2, 3), LIFE);
    EXPECT_EQ(Life.isLife(1, 4), LIFE);
    EXPECT_EQ(Life.isLife(3, 0), LIFE);
    EXPECT_EQ(Life.isLife(4, 0), LIFE);
    EXPECT_EQ(Life.isLife(0, 1), LIFE);

}

TEST(TestForDomain, TestOneIterForInternalCells){
    Configuration Config (1, 3, 2, 3, 5, 5);
    Game Life(Config);

    Field field(5, 5);
    //.....
    //.**..
    //.**..
    //..*..
    //.....
    field.setLife(1, 1);
    field.setLife(1, 2);
    field.setLife(2, 1);
    field.setLife(2, 2);
    field.setLife(2, 3);

    Life.setField(field);
    Life.OneIteration();
    //.....
    //.**..
    //...*.
    //.**..
    //.....
    EXPECT_EQ(Life.isLife(1, 1), LIFE);
    EXPECT_EQ(Life.isLife(1, 2), DEAD);
    EXPECT_EQ(Life.isLife(2, 1), LIFE);
    EXPECT_EQ(Life.isLife(2, 2), DEAD);
    EXPECT_EQ(Life.isLife(2, 3), LIFE);
    EXPECT_EQ(Life.isLife(3, 2), LIFE);
    EXPECT_EQ(Life.isLife(1, 3), LIFE);

}

int main(int argc, char** argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

