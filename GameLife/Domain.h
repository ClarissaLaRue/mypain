#ifndef GAMELIFE_DOMAIN_H
#define GAMELIFE_DOMAIN_H
#include <vector>
#include <string>
#include <fstream>
#include "LifeParser.h"
#include "GameLifeErrors.h"
#include "Cell.h"
#include "Field.h"

using std::string;
using std::vector;
using std::cin;
using std::fstream;

class Configuration;

class Game{
public:
    Game();
    Game(Configuration Config);
    void setField(const Field& other);
    void OneIteration();
    void Step(int count);
    int isLife(int X, int Y);
    int lifeNeighbors(int X, int Y);
    int DeadNeighbors(int X, int Y);
    void Printer ();
    void PrinterForFile (fstream & os);
    void loadFromFile(fstream & is);
    void setLife(int X, int Y);
    void clear(int X, int Y);
    void reset();
    void newRules(int m, int n, int k);
    ~Game();
private:
    Field current;
    int M;
    int N;
    int K;
    int fieldX;
    int fieldY;
};

#endif //GAMELIFE_DOMAIN_H