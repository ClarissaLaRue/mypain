#ifndef GAMELIFE_CELL_H
#define GAMELIFE_CELL_H
#include <fstream>
#include <iostream>

using std::fstream;
using std::cout;

const int LIFE = 1;
const int DEAD = 0;

class Cell{
    int state;
public:
    Cell();
    Cell& operator=(const Cell& other);

    int isLife();
    void setLife();
    void setDead();
    void Printer ();
    void PrinterForFile (fstream & os);
    ~Cell();
};

#endif //GAMELIFE_CELL_H
