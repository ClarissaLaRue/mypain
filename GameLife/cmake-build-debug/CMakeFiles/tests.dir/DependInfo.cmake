# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "C:/All_labs/Course2/GameLife/Driver.cpp" "C:/All_labs/Course2/GameLife/cmake-build-debug/CMakeFiles/tests.dir/Driver.cpp.obj"
  "C:/All_labs/Course2/GameLife/Parser.cpp" "C:/All_labs/Course2/GameLife/cmake-build-debug/CMakeFiles/tests.dir/Parser.cpp.obj"
  "C:/All_labs/Course2/GameLife/TestMain.cpp" "C:/All_labs/Course2/GameLife/cmake-build-debug/CMakeFiles/tests.dir/TestMain.cpp.obj"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../lib/googletest-release-1.6.0/include"
  "../lib/googletest-release-1.6.0"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "C:/All_labs/Course2/GameLife/cmake-build-debug/lib/googletest-release-1.6.0/CMakeFiles/gtest.dir/DependInfo.cmake"
  "C:/All_labs/Course2/GameLife/cmake-build-debug/lib/googletest-release-1.6.0/CMakeFiles/gtest_main.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
