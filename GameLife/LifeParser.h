#ifndef GAMELIFE_LIFEPARSER_H
#define GAMELIFE_LIFEPARSER_H

#include <vector>
#include <string>
#include <fstream>
#include <algorithm>
#include <io.h>
#include "Driver.h"
#include "Parser.h"
#include "GameLifeErrors.h"
#include "Cell.h"
#include "Field.h"

using std::vector;
using std::string;
const string NOFILE = "NoFile";
const string NOForm = "0";
const string LongForm_INPUT = "--input";
const string ShortForm_INPUT = "-if";
const string SPACE = " ";
const int NOTHING = 0;

const int RESET = 0;
const int SET = 1;
const int CLEAR = 2;
const int STEP = 3;
const int BACK = 4;
const int SAVE = 5;
const int LOAD = 6;
const int RULES = 7;

//--------------------------------Классы
class Configuration{
    int iteration;
    int M;
    int N;
    int K;
    int fieldX;
    int fieldY;
    Field startField;
    string inputFile;
    string outputFile;
public:
    Configuration();
    Configuration(int i, int M, int N, int K, int fieldX, int fieldY);
    Configuration& operator= (const Configuration& other);
    void makeConfig(Variant Game);
    void fullfield(string OptName, string arg);
    void addfiles(Variant Game);
    void setField(Variant Game);
    int getM();
    int getN();
    int getK();
    int getfieldX();
    int getfieldY();
    Field getField();
    string getInputFile();
    string getOutputFile();
    int getIteration();
};

class Instruction{
    int keyInstruction;
    int Arg1;
    int Arg2;
    int Arg3;
    string Arg4;
public:
    Instruction();
    Instruction(int keyI, int Arg1, int Arg2, int Arg3, string Arg4);
    int getKey();
    int getArg1();
    int getArg2();
    int getArg3();
    string getArg4();
    ~Instruction();
};

class ConfigurationInter{
    vector<Instruction> instrustions;
public:
    ConfigurationInter();
    ~ConfigurationInter();
    void makeConfig(Variant Game);
    ConfigurationInter& operator= (const ConfigurationInter& other);
    int getSize();
    int getKey(int pos);
    int getArg1(int pos);
    int getArg2(int pos);
    int getArg3(int pos);
    string getArg4(int pos);
private:
    int defaultCapacity;
    int capacity;
    int size;
};

//--------------------------------Функции
Configuration lifeParserAuto(int count, vector<string> Args);
ConfigurationInter lifeParserInter(vector<string> Args);
Configuration lifeParserFile(string file);
void addAllOptions(Rules &GameRules);
void addInterOptions(Rules &GameRules);
void addOption(Rules &GameRules, string LongForm, string ShortForm,
               int argCount, int argType);
void addAllOptionsForFile(Rules &GameRules);
vector<string> makeStringFromFile(string input);
void isCorrectFileParse(Variant &ArgFile);
vector<string> makeString(string str);

#endif //GAMELIFE_LIFEPARSER_H
