#ifndef GAMELIFE_DRIVER_H
#define GAMELIFE_DRIVER_H

#include <vector>
#include <string>
#include <fstream>
#include <stack>
#include "GameLifeErrors.h"
#include "LifeParser.h"
#include "Domain.h"

using std::string;
using std::vector;
using std::fstream;
using std::stack;

class Configuration;
class Game;
class ConfigurationInter;

//--------------------------------Функции
void GameLife(int count, char  *argv[]);
void DoAuto(int count, vector<string> Args);
void DoInteractive();
int DoInstruction(Game& GameLife, ConfigurationInter Config, int n);
#endif //GAMELIFE_DRIVER_H
