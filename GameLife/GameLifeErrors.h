#ifndef GAMELIFE_GAMELIFEERRORS_H
#define GAMELIFE_GAMELIFEERRORS_H

class FileNotFound : std::exception{};

class CanNotOpenFile : std::exception{};

class ParseFileError : std::exception{};

class Helper: std::exception{};

class BackError: std::exception{};


#endif //GAMELIFE_GAMELIFEERRORS_H
