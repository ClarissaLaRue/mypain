#include "Field.h"

Field::Field() : defaultCapacity(0), capacity(0), fieldX(0), fieldY(0) {
    field.resize(defaultCapacity, vector<Cell>());
}

Field::Field(int X, int Y){
    field.resize(X);
    for(int i = 0; i < X; i++){
        field[i].resize(Y);
    }
    fieldX = X;
    fieldY = Y;
}

Field& Field::operator= (const Field& other){
    if (this == &other)
        return *this;
    field = other.field;
    return *this;
}

int Field::isLife(int X, int Y){
    return  field[X][Y].isLife();
}

//Установить живую клетку в X Y
void Field::setLife(int X, int Y){
    if ((X > fieldX) || (Y > fieldY)){
        return;
    }
    field[X][Y].setLife();
}

//Установить мертвую клетку в X Y
void Field::setDead(int X, int Y){
    if ((X > fieldX) || (Y > fieldY)){
        return;
    }
    field[X][Y].setDead();
}

//Количество мертвых соседей
int Field::DeadNeighbors(int X, int Y) {
    return 8 - lifeNeighbors(X, Y);
}

//Количество живых соседей
int Field::lifeNeighbors(int X, int Y){
    int count = 0;
    for (int i = X-1; i <= X+1; i++){
        for (int j = Y-1; j<=Y+1; j++){
            if((i != X ) || (j != Y)){
                if (getCell(i,j).isLife()){
                    count++;
                }
            }
        }
    }
    return count;
}

//Возвращает ячейку по координам X Y,
// переходя на крайние если вышли за границы
Cell Field::getCell(int X, int Y){
    if(X<0){
        X=fieldX-1;
    }
    if (X>fieldX-1){
        X=0;
    }
    if (Y<0){
        Y=fieldY-1;
    }
    if (Y>fieldY-1){
        Y=0;
    }
    return field[X][Y];
}

//Вывод
void Field::Printer (){
    for (int i = 0; i < fieldY; i++){
        for(int j = 0; j < fieldX; j++){
            field[j][i].Printer();
        }
        cout << std::endl;
    }
}

void Field::PrinterForFile (fstream & os){
    for (int i = 0; i < fieldY; i++){
        for(int j = 0; j < fieldX; j++){
            field[j][i].PrinterForFile(os);
        }
        os << std::endl;
    }
}

Field::~Field(){}