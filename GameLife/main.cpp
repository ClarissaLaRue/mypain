#include <iostream>
#include "Driver.h"
#include "Parser.h"
#include "LifeParser.h"


int main(int argc, char* argv[]) {
    try{
        GameLife(argc, argv);
    }
    catch (const ExactMistmach& exactMistmach){
        cout << "Failed to parse command line arguments: use --help or -h to get help";
    }
    catch (const Helper& helper){
    }
    catch (const NoMatchHint& noMatchHint){
    }
    catch (const FileNotFound& fileNotFound){
        cout << "Not found input file";
    }
    catch (const CanNotOpenFile& canNotOpenFile){
        cout << "Can not open file";
    }
    catch (const ParseFileError& parseFileError){
        cout << "File parse error";
    }
    catch (const BackError& backError){
        cout << "Can not to do back";
    }
    return 0;
}