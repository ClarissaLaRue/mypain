#ifndef GAMELIFE_FIELD_H
#define GAMELIFE_FIELD_H
#include <vector>
#include <string>
#include <fstream>
#include "Cell.h"

using std::string;
using std::vector;
using std::fstream;

class Field{
    vector<vector<Cell>> field;
public:
    Field();
    Field(int X, int Y);
    Field& operator= (const Field& other);
    int isLife(int X, int Y);
    void setLife(int X, int Y);
    void setDead(int X, int Y);
    int lifeNeighbors(int X, int Y);
    int DeadNeighbors(int X, int Y);
    void Printer ();
    void PrinterForFile (fstream & os);
    ~Field();
private:
    int defaultCapacity;
    int capacity;
    int fieldX;
    int fieldY;
    Cell getCell(int X, int Y);
};


#endif //GAMELIFE_FIELD_H
