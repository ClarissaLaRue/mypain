
#include "LifeParser.h"
//--------------------------------Configuration
Configuration::Configuration() : iteration(10), M(3), N(2), K(3), fieldX(10), fieldY(10),
                                 startField(10, 10),
                                 inputFile(NOFILE), outputFile(NOFILE) {}

Configuration::Configuration(int i, int M, int N, int K, int X, int Y) :
        iteration(i), M(M), N(N), K(K), fieldX(X), fieldY(Y), startField(X, Y),
        inputFile(NOFILE), outputFile(NOFILE)
{}

Configuration& Configuration::operator= (const Configuration& other){
    iteration = other.iteration;
    M = other.M;
    N = other.N;
    K = other.K;
    fieldX = other.fieldX;
    fieldY = other.fieldY;
    startField = startField;
    return *this;
}

void Configuration::makeConfig(Variant Game) {
    for(int i=0; i<Game.getSizeROpt(); i++){
        string OptName = Game.getRName(i);
        string arg = Game.getRArg(OptName);
        fullfield(OptName, arg);
    }
}

void Configuration::fullfield(string OptName, string arg) {
    if ((OptName == "--iterations") || (OptName == "-ic")) {
        iteration = stoi(arg);
    }
    if ((OptName == "--field") || (OptName =="-f")){
        size_t i = arg.find(" ");
        fieldX = stoi(arg.substr(0, i));
        fieldY = stoi(arg.substr(i, arg.length()));
    }
    if (OptName == "-m"){
        M = stoi(arg);
    }
    if (OptName == "-n"){
        N = stoi(arg);
    }
    if (OptName == "-k"){
        K = stoi(arg);
    }
}

void Configuration::addfiles(Variant Game){
    for (int i = 0; i<Game.getSizeROpt(); i++){
        string OptName = Game.getRName(i);
        if ((OptName == "--input") || (OptName == "-if")){
            inputFile = Game.getRArg(OptName);
        }
        if ((OptName == "--output") || (OptName == "-o")){
            outputFile = Game.getRArg(OptName);
        }
    }
    if (inputFile == NOFILE){
        throw FileNotFound();
    }
}

void Configuration::setField(Variant Game){
    for (int i = 0; i<Game.getSizeROpt(); i++){
        string OptName = Game.getRName(i);
        if (OptName == "SET"){
            string arg = Game.getRArg(OptName);
            size_t j = arg.find(" ");
            int X = stoi(arg.substr(0, j));
            int Y = stoi(arg.substr(j, arg.length()));
            startField.setLife(X, Y);
        }
    }
}

int Configuration::getM(){
    return M;
}

int Configuration::getN(){
    return N;
}

int Configuration::getK(){
    return K;
}

int Configuration::getfieldX(){
    return fieldX;
}

int Configuration::getfieldY(){
    return fieldY;
}

Field Configuration::getField(){
    return startField;
}

string Configuration::getInputFile(){
    return inputFile;
}

string Configuration::getOutputFile(){
    return outputFile;
}

int Configuration::getIteration(){
    return iteration;
}
//--------------------------------ConfigurationInter
ConfigurationInter::ConfigurationInter(): defaultCapacity(0), capacity(0), size(0) {
    instrustions.resize(defaultCapacity);}

ConfigurationInter::~ConfigurationInter(){}

void ConfigurationInter::makeConfig(Variant Game){
    instrustions.resize(Game.getSizeROpt());
    for(int i=0; i<Game.getSizeROpt(); i++){
        capacity++;
        instrustions[size] = Instruction();
        string OptName = Game.getRName(i);
        string arg = Game.getRArg(OptName);
        if (OptName == "reset"){
            instrustions[size] = Instruction(RESET, NOTHING, NOTHING, NOTHING, SPACE);
        }
        if (OptName == "set"){
            size_t s = arg.find(" ");
            int X = stoi(arg.substr(0, s));
            int Y = stoi(arg.substr(s, arg.length()));
            instrustions[size] = Instruction(SET, X, Y, NOTHING, SPACE);
        }
        if (OptName == "clear"){
            size_t s = arg.find(" ");
            int X = stoi(arg.substr(0, s));
            int Y = stoi(arg.substr(s, arg.length()));
            instrustions[size] = Instruction(CLEAR, X, Y, NOTHING, SPACE);
        }
        if (OptName == "step"){
            instrustions[size] = Instruction(STEP, stoi(arg), NOTHING, NOTHING, SPACE);
        }
        if (OptName == "back"){
            instrustions[size] = Instruction(BACK, NOTHING, NOTHING, NOTHING, SPACE);
        }
        if (OptName == "save"){
            instrustions[size] = Instruction(SAVE, NOTHING, NOTHING, NOTHING, arg);
        }
        if (OptName == "load"){
            instrustions[size] = Instruction(LOAD, NOTHING, NOTHING, NOTHING, arg);
        }
        if (OptName == "rules"){
            size_t s = arg.find(" ");
            int m = stoi(arg.substr(0, s));
            size_t h = arg.find(" ", s+1);
            int n = stoi(arg.substr(s, h));
            int k = stoi(arg.substr(h, arg.length()));
            instrustions[size] = Instruction(RULES, m, n, k, SPACE);
        }
        size++;
    }
}

ConfigurationInter& ConfigurationInter::operator= (const ConfigurationInter& other){
    if (this == &other)
        return *this;
    instrustions = other.instrustions;
    capacity = other.capacity;
    defaultCapacity = other.defaultCapacity;
    size = other.size;
    return *this;
}

int ConfigurationInter::getKey(int pos){
    return instrustions[pos].getKey();
}

int ConfigurationInter::getArg1(int pos){
    return instrustions[pos].getArg1();
}

int ConfigurationInter::getArg2(int pos){
    return instrustions[pos].getArg2();
}

int ConfigurationInter::getArg3(int pos){
    return instrustions[pos].getArg3();
}

string ConfigurationInter::getArg4(int pos){
    return instrustions[pos].getArg4();
}

int ConfigurationInter::getSize(){
    return size;
}
//--------------------------------Instruction
Instruction::Instruction(){}

Instruction::Instruction(int keyI, int Arg1, int Arg2, int Arg3, string Arg4):
        keyInstruction(keyI), Arg1(Arg1), Arg2(Arg2), Arg3(Arg3), Arg4(Arg4) {}

int Instruction::getKey(){
    return keyInstruction;
}

int Instruction::getArg1(){
    return Arg1;
}

int Instruction::getArg2(){
    return Arg2;
}

int Instruction::getArg3(){
    return Arg3;
}

string Instruction::getArg4(){
    return Arg4;
}

Instruction::~Instruction(){}
//--------------------------------Функции
Configuration lifeParserAuto(int count, vector<string> Args){
    Configuration GameConfig;
    Rules GameRules;
    Rules FileRules;
    Variant ArgStr;
    Variant ArgFile;

    //Парсинг аргументов
    addAllOptions(GameRules);
    addAllOptionsForFile(FileRules);
    ArgStr = Parse(GameRules, count, Args);
    //Парсинг файла
    GameConfig.addfiles(ArgStr);
    string input = GameConfig.getInputFile();


    vector<string> fileString = makeStringFromFile(input);
    ArgFile = Parse(FileRules, fileString.size(), fileString);
    isCorrectFileParse(ArgFile);
    GameConfig.makeConfig(ArgFile);
    GameConfig.makeConfig(ArgStr);
    return GameConfig;
}

//Считывание с файла состояния поля
Configuration lifeParserFile(string file){
    Configuration GameConfig;
    Rules FileRules;
    Variant ArgFile;
    addOption(FileRules, "SET", NOForm, 2, INT);
    vector<string> fileString = makeStringFromFile(file);
    ArgFile = Parse(FileRules, fileString.size(), fileString);
    isCorrectFileParse(ArgFile);
    GameConfig.makeConfig(ArgFile);
    return GameConfig;
}

ConfigurationInter lifeParserInter(vector<string> Args){
    ConfigurationInter GameConfig;
    Rules GameRules;
    Variant Arg;
    addInterOptions(GameRules);
    Arg = Parse(GameRules, Args.size() , Args);
    GameConfig.makeConfig(Arg);
    return GameConfig;
}

//Добавляем опции для парсинга строки
void addAllOptions(Rules &GameRules){
    addOption(GameRules, LongForm_INPUT, ShortForm_INPUT, 1, STRING);
    addOption(GameRules, "--output", "-o", 1, STRING);
    addOption(GameRules, "--iterations", "-ic", 1, INT);
    addOption(GameRules, "--field", "-f", 2, INT);
    addOption(GameRules, "-m", NOForm, 1, INT);
    addOption(GameRules, "-n", NOForm, 1, INT);
    addOption(GameRules, "-k", NOForm, 1, INT);
    addOption(GameRules, "--help", "-h", 0, NO);
}

//Опции для парсинга интерактивного режима
void addInterOptions(Rules &GameRules){
    addOption(GameRules, "reset", NOForm, 0, NO);
    addOption(GameRules, "set", NOForm, 2, INT);
    addOption(GameRules, "clear", NOForm, 2, INT);
    addOption(GameRules, "step", NOForm, 1, INT);
    addOption(GameRules, "step", NOForm, 0, INT);
    addOption(GameRules, "back", NOForm, 0, NO);
    addOption(GameRules, "save", NOForm, 1, STRING);
    addOption(GameRules, "load", NOForm, 1, STRING);
    addOption(GameRules, "rules", NOForm, 3, INT);
}

//Получаем вектор строк с командами из файла
vector<string> makeStringFromFile(string input){
    vector<string> FileAgr;
    std::fstream inputF;
    inputF.open(input);
    if (!inputF.is_open()){
        throw CanNotOpenFile();
    }
    string str;
    while (!inputF.eof()) {
        getline(inputF, str);
        makeString(str);
    }
    return FileAgr;
}

//Преобразование строки в вектор
vector<string> makeString(string str){
    vector<string> Arg;
    size_t index = str.find('#'); //избавляемся от символов после решотки
    str.substr(0, index);
    //удаление пробелов
    std::unique(str.begin(), str.end(), [](char l, char r){
        return std::isspace(l) && std::isspace(r) && l == r;
    });
    //удаление мусорных символов в конце строки
    index = str.find('x');
    str.resize(index);
    int n = std::count(str.begin(), str.end(), ' ');
    Arg.reserve(Arg.capacity() + (n+1));
    while (str.length() > 0){
        //Считаем количество пробелов и резервируем память в векторе
        // Нужное количество памяти для слов(пробелы + 1)
        index = str.find(' ');
        Arg.push_back(str.substr(0, index));
        str = str.substr(index, str.length());
    }
    return Arg;
}

//Добавление опций для парсера файла
void addAllOptionsForFile(Rules &GameRules){
    addOption(GameRules, "FIELD", NOForm, 2, INT);
    addOption(GameRules, "RULES", NOForm, 3, INT);
    addOption(GameRules, "SET", NOForm, 2, INT);
}

//Добавление одной опции
void addOption(Rules &GameRules, string LongForm, string ShortForm, int argCount, int argType){
    GameRules.addOpt(LongForm, ShortForm, argCount, argType);
}

//Проверка на повторение команд в файле
void isCorrectFileParse(Variant &ArgFile){
    int countRules = 0;
    int countField = 0;
    for(int i=0; i<ArgFile.getSizeROpt(); i++){
        if (ArgFile.getRName(i) == "FIELD"){
            countField++;
        }
        if (ArgFile.getRName(i) == "RULES"){
            countRules++;
        }
    }
    if ((countField > 1) || (countRules > 1)){
        throw ParseFileError();
    }
}



