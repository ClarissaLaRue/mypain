#include "Driver.h"


//--------------------------------Классы

//--------------------------------Управление
void GameLife(int count, char *argv[]){
    if(count  > 1){
        vector<string> Args;
        for (int i = 1; i < count; i++) {
            Args[i] = argv[i];
        }
        DoAuto(count-1, Args);
    }else{
        DoInteractive();
    }
}

//Автоматическая игра
void DoAuto(int count, vector<string> Args){
    Configuration Config = lifeParserAuto(count, Args);
    Game GameLife(Config);
    GameLife.Step(Config.getIteration());
    fstream outFile(Config.getOutputFile());
    GameLife.PrinterForFile(outFile);
}

//Интерактивная игра
void DoInteractive(){
    Game GameLife;
    stack<Game> State;
    State.push(GameLife);
    while (1){
        string str;
        cin >> str;
        vector<string> instruction(makeString(str));
        ConfigurationInter Config = lifeParserInter(instruction);
        for (int i = 0; i< Config.getSize(); i++){
            int k = DoInstruction(GameLife, Config, i);
            if (k){
                if (State.size() == 1){
                    throw BackError();
                }
                State.pop();
                GameLife = State.top();
            } else{
                State.push(GameLife);
            }
            GameLife.Printer();
        }
    }
}

//Выполняем одну команду
int DoInstruction(Game& GameLife,ConfigurationInter Config, int n){
    int thisKey = Config.getKey(n);
    if (thisKey == RESET){
        GameLife.reset();
    }
    if (thisKey == SET){
        GameLife.setLife(Config.getArg1(n),Config.getArg2(n));
    }
    if (thisKey == CLEAR){
        GameLife.clear(Config.getArg1(n),Config.getArg2(n));
    }
    if (thisKey == STEP){
        GameLife.Step(Config.getArg1(n));
    }
    if (thisKey == BACK){
        return 1;
    }
    if (thisKey == SAVE ){
        std::fstream inputF;
        inputF.open((Config.getArg4(n)));
        if (!inputF.is_open()){
            throw CanNotOpenFile();
        }
        GameLife.PrinterForFile(inputF);
    }
    if (thisKey == LOAD ){
        Configuration ParseFile = lifeParserFile(Config.getArg4(n));
        GameLife.setField(ParseFile.getField());

    }
    if (thisKey == RULES ){
        GameLife.newRules(Config.getArg1(n),Config.getArg2(n),Config.getArg3(n));
    }
    return 0;
}