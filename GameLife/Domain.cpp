#include "Domain.h"

//--------------------------------Классы
//--------------------------------Game
Game::Game() {}

Game::Game(Configuration Config) : current(Config.getField()),
                                   M(Config.getM()), N(Config.getN()), K(Config.getK()),
                                   fieldX(Config.getfieldX()), fieldY(Config.getfieldY()) {}


void Game::setField(const Field& other){
    current = other;
}

//Смена одного поколения
void Game::OneIteration(){
    Field next(fieldX, fieldY);
    for(int i = 0; i < fieldX; i++){
        for (int j = 0; j < fieldY; j++){
            int lifeN = current.lifeNeighbors(i,j);
            int stateCell = current.isLife(i, j);
            if ((stateCell == DEAD) && (lifeN == M)){
                next.setLife(i, j);
            }
            if ((stateCell == LIFE) && (lifeN < N)){
                next.setDead(i, j);
            }
            if ((stateCell == LIFE) && (lifeN > K)){
                next.setDead(i, j);
            }
            if ((stateCell == LIFE) && (lifeN >= N) && (lifeN <= K)){
                next.setLife(i, j);
            }
        }
    }
    current = next;
}

//Выполняем N шагов
void Game::Step(int count){
    for (int i = 0; i < count ; i++){
        OneIteration();
    }
}

//Проверка, живая ли клетка
int Game::isLife(int X, int Y){
    return current.isLife(X, Y);
}

//Количество живых соседей
int Game::lifeNeighbors(int X, int Y){
    return current.lifeNeighbors(X, Y);
}

//Количество мертвых соседей
int Game::DeadNeighbors(int X, int Y){
    return current.DeadNeighbors(X, Y);
}

//Вывод
void Game::Printer (){
    cout << endl;
    current.Printer();
}

void Game::PrinterForFile (fstream & os){
    current.PrinterForFile(os);
}

//Установить организм в клетку
void Game::setLife(int X, int Y){
    current.setLife(X, Y);
}

//Очистить клетку
void Game::clear(int X, int Y){
    current.setDead(X, Y);
}

//Очищаем поле
void Game::reset(){
    for(int i =0; i<fieldY; i++){
        for(int j=0; j<fieldX; j++){
            clear(j,i);
        }
    }
}

//Установка новых правил
void Game::newRules(int m, int n, int k){
    M=m;
    N=n;
    K=k;
}

Game::~Game() {}
