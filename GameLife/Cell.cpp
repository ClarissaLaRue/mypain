#include "Cell.h"
//--------------------------------Cell
Cell::Cell() : state(DEAD) {}

Cell& Cell::operator=(const Cell &other) {
    state = other.state;
    return *this;
}

//Сделать живой
void Cell::setLife() {
    state = LIFE;
}

//Сделать мертвой
void Cell::setDead(){
    state = DEAD;
}

//Проверка живая ли клетка
int Cell::isLife(){
    if (state == LIFE)
        return LIFE;
    return DEAD;
}

//Вывод состояния
void Cell::Printer (){
    if (state == LIFE){
        cout << "*";
    }else{
        cout << ".";
    }
}

void Cell::PrinterForFile (fstream & os){
    if (state == LIFE){
        os << "*";
    }else{
        os << ".";
    }
}

Cell::~Cell(){}