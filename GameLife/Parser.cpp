#include "Parser.h"

//--------------------------------Парсинг аргументов
Variant Parse(Rules ThisRules, int count, vector<string> Args) {
    Variant Game(ThisRules);
    for (int i = 0; i < count; i++) {
        Game.ParseOption(Args, i);//парсим один аргумент
        i += Game.getArgCount(Args[i]);
    }
    return Game;
}

//--------------------------------Rules
Rules::Rules() : defaultCapacity(0), capacity(0), size(0) {
    allOptions.resize(defaultCapacity);
}

Rules& Rules::operator=(const Rules& other){
    if (this == &other)
        return *this;
    allOptions = other.allOptions;
    capacity = other.capacity;
    defaultCapacity = other.defaultCapacity;
    size = other.size;
    return *this;
}

//Добавление новой опции в правила
void Rules::addOpt(string LongForm, string ShortForm, int argCount, int argType) {
    capacity++;
    allOptions.resize(capacity);
    allOptions[size] = Opt(LongForm, ShortForm, argCount, argType);
    size++;
}

//Вывод сообщение о всех опциях
void Rules::getHelp() {
    for (auto i = 0; i < size ; i++){
        cout << getHelpMessage(allOptions[i].LongForm)<< endl;
    }
}

int Rules::getSize(){
    return size;
};

//Проверка на наличие опции
int Rules::isOpt(string arg) {
    for (int i = 0; i < size; i++) {
        if ((allOptions[i].LongForm == "--help") &&
            ((allOptions[i].LongForm == arg) || (allOptions[i].ShortForm == arg))) {
            return HELP;
        }
        if ((allOptions[i].LongForm == arg) || (allOptions[i].ShortForm == arg)) {
            return 1;
        }
    }
    return 0;
}

//Получаем количество аргументов
int Rules::getArgCount(string arg) {
    for (int i = 0; i < size; i++) {
        if ((allOptions[i].LongForm == arg) || (allOptions[i].ShortForm == arg)) {
            return allOptions[i].argCount;
        }
    }
    return 0;
}

//Получае тип аргументов
int Rules::getArgType(string arg) {
    for (int i = 0; i < size; i++) {
        if ((allOptions[i].LongForm == arg) || (allOptions[i].ShortForm == arg)) {
            return allOptions[i].argType;
        }
    }
    return 0;
}

//Получаем описание опции
string Rules::getHelpMessage(string arg) {
    for (int i = 0; i < size; i++) {
        if ((allOptions[i].LongForm == arg) || (allOptions[i].ShortForm == arg)) {
            return allOptions[i].getHint();
        }
    }
    return "can not write Help Massage";
}

string Rules::getArgName(int pos){
    if(pos > size){
        return "No Option";
    }
    return "LongForm " + allOptions[pos].LongForm + " ShortForm " + allOptions[pos].ShortForm;
};

string Rules::getArgCount(int pos){
    if(pos > size){
        return "No Option";
    }
    return std::to_string(allOptions[pos].argCount);
};

string Rules::getArgType(int pos){
    if(pos > size){
        return "No Option";
    }
    return allOptions[pos].getType();
};

//Повторяется ли инструкция
int Rules::isDouble(string arg){
    for(int i = 0; i < size-1; i++){
        if ((allOptions[i].LongForm == arg) && (allOptions[i+1].LongForm == arg)){
            return 1;
        }
    }
    return 0;
}

int Rules::getArgSecondCount(string arg){
    for(int i = 0; i < size-1; i++){
        if ((allOptions[i].LongForm == arg) && (allOptions[i+1].LongForm == arg)){
            return allOptions[i].argCount;
        }
    }
    return 0;
}

//--------------------------------Variant
Variant::Variant() {}

Variant &Variant::operator=(const Variant& other) {
    if (this == &other)
        return *this;
    actualOptions = other.actualOptions;
    noMatchHint = other.noMatchHint;
    exactMistmach = other.exactMistmach;
    rule = other.rule;
    return *this;
}

Variant::Variant(Rules ThisRules) : rule(ThisRules) {}

//Парсинг одной опции
void Variant::ParseOption(vector<string> Args, int pos) {
    int is = isOpt(Args[pos]);
    if (is == 0) {//Завершение, если нет совпадения
        throw ExactMistmach();
    }
    //Если опция help выводим сообщение на экран
    if (is == HELP) {
        rule.getHelp();
        throw Helper();
    }
    //Достаем количество и тип аргументов
    int count = getArgCount(Args[pos]);
    int type = getArgType(Args[pos]);
    int i = 1;
    if (isDouble(Args[pos])){
        int secondCount = getArgSecondCount(Args[pos]);
        if (type == INT){
            is = isIntArg(Args[pos + 1]);
            if ((is == 1) && (secondCount == 0)){
                actualOptions.DoROpt(Args[pos], "1", INT);
                actualOptions.flag = 1;
            }
        }
    }else{
        while (count > 0) {
            if (type == INT) {
                is = isIntArg(Args[pos + i]);
                if (is == 1) {
                    cout << getHelpMessage(Args[pos]);
                    throw noMatchHint;
                }
                string arg = Args[pos + i];
                actualOptions.DoROpt(Args[pos], arg, INT);
            }
            if (type == STRING) {
                string arg = Args[pos + i];
                actualOptions.DoROpt(Args[pos], arg, STRING);
            }
            actualOptions.flag = 1;
            count--;
            i++;
        }
    }

}

//Проверка существования опции
int Variant::isOpt(string arg) {
    return rule.isOpt(arg);
}

//Получаем количество аргументов опции
int Variant::getArgCount(string arg) {
    return rule.getArgCount(arg);
}

int Variant::isDouble(string arg){
    return rule.isDouble(arg);
}

int Variant::getArgSecondCount(string arg){
    return rule.getArgSecondCount(arg);
}

//Получаем тип аргументов опции
int Variant::getArgType(string arg) {
    return rule.getArgType(arg);
}

//Получаем количество правильных опций
int Variant::getSizeROpt() {
    return actualOptions.size;
}

//Получаем имя правильной опции в данной позиции
string Variant::getRName(int pos){
    return actualOptions.getName(pos);
}

//Получаем аргументы правильной опции в данной позиции
string Variant::getRArg(string name){
    return actualOptions.getArg(name);
}

//Получаем описание опции
string Variant::getHelpMessage(string arg) {
    return rule.getHelpMessage(arg);
}

//Проверка на корректность аргумента, если он типа Int
int Variant::isIntArg(string arg) {
    for (size_t i = 0; i < arg.length(); i++) {
        char ch = arg[i];
        if ((ch != '0') && (ch != '1') && (ch != '2') && (ch != '3') && (ch != '4') &&
            (ch != '5') && (ch != '6') && (ch != '7') && (ch != '8') && (ch != '9')) {
            return 1;
        }
    }
    return 0;
}

Variant::~Variant() {}

//--------------------------------ActualOptions
ActualOptions::ActualOptions() : flag(0), defaultCapacity(0), capacity(0), size(0) {
    ROptions.resize(defaultCapacity);
}

ActualOptions& ActualOptions::operator=(const ActualOptions& other){
    if (this == &other)
        return *this;
    flag = other.flag;
    ROptions = other.ROptions;
    capacity = other.capacity;
    defaultCapacity = other.defaultCapacity;
    size = other.size;
    return *this;
}

//Добавляем Opt с аргументами в массив правильных опций
void ActualOptions::DoROpt(string opt, string arg, int type) {
    for (int i = 0; i < size; i++) {//если Opt уже существует и надо добавить ещё один аргумент
        if (ROptions[i].name == opt) {
            ROptions[i].arg += " ";
            ROptions[i].arg += arg;
            string str = ROptions[i].arg;
            ROptions[i] = Option(opt, str, type);
            return;
        }
    }
    capacity++;
    ROptions.resize(capacity);
    ROptions[size] = Option(opt, arg, type);
    size++;
}

//Получаем имя опции на данной позиции
string ActualOptions::getName(int pos){
    return  ROptions[pos].name;
}

//Получаем аргументы по имени
string ActualOptions::getArg(string name){
    for(int i=0; i<size; i++){
        if (ROptions[i].name == name)
            return ROptions[i].arg;
    }
    return "0";
}

ActualOptions::~ActualOptions() {}

//--------------------------------NoMatchHint
NoMatchHint::NoMatchHint() {}

NoMatchHint& NoMatchHint::operator=(const NoMatchHint& other){
    if (this == &other)
        return *this;
    return *this;
}

NoMatchHint::~NoMatchHint() {}

//--------------------------------ExactMistmach
ExactMistmach::ExactMistmach() {}

ExactMistmach&  ExactMistmach::operator=(const ExactMistmach& other){
    if (this == &other)
        return *this;
    return *this;
}

ExactMistmach::~ExactMistmach() {}

//--------------------------------Opt
Opt::Opt() {}

Opt::Opt(string LongForm, string ShortForm, int argCount, int argType) :
        LongForm(LongForm), ShortForm(ShortForm) {
    this->argCount = argCount;
    this->argType = argType;
}

//Описание опции в строковом виде
string Opt::getHint() {
    string str = "Flag: ";
    if (ShortForm == "0") {
        str += LongForm;
    } else {
        str += " LongForm ";
        str += LongForm;
        str += " ShortForm ";
        str += ShortForm;
    }
    if (argCount > 0) {
        string strT = getType();
        string strC = std::to_string(argCount);
        str += " Expected type of arguments: ";
        str += strT;
        str += " Expected count of arguments: ";
        str += strC;
    } else {
        str += " no arguments ";
    }
    return str;
}

//Получение типа аргументов опции в строковом виде
string Opt::getType() {
    if (argType == INT) {
        return "INT";
    }
    if (argType == STRING) {
        return "STRING";
    }
    return "0";
}

//--------------------------------Option
Option::Option() {}

Option::Option(string opt, string arg, int type) : name(opt), arg(arg), type(type) {
    count++;
}

Option::~Option() {}