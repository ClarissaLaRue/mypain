#ifndef GAMELIFE_PARSER_H
#define GAMELIFE_PARSER_H

#include <iostream>
#include <vector>
#include <string>
#include <exception>
#include <unordered_map>
#include "GameLifeErrors.h"

using std::string;
using std::vector;
using std::cout;
using std::endl;

const int STRING = 2;
const int HELP = 2;
const int INT = 1;
const int NO = 0;


//--------------------------------Классы
class ActualOptions;
class NoMatchHint;
class ExactMistmach;
class Rules;
class Opt;
class Option;
class WrongOption;

class Rules{
    vector<Opt> allOptions;
public:
    friend class Variant;
    friend class NoMatchHint;

    Rules();
    Rules& operator=(const Rules& other);
    //копирующий конструктор
    void addOpt(string LongForm, string ShortForm, int argCount, int argType);
    void getHelp();
    int isOpt(string arg);
    int getArgCount(string arg);
    int getArgType(string arg);
    string getArgName(int pos);
    string getArgCount(int pos);
    string getArgType(int pos);
    string getHelpMessage(string arg);
    int isDouble(string arg);
    int getArgSecondCount(string arg);
    int getSize();
private:
    int defaultCapacity;
    int capacity;
    int size;
};

class ActualOptions{
    int flag;
    vector<Option> ROptions;
public:
    friend class Variant;
    friend class Option;

    ActualOptions();
    ActualOptions& operator=(const ActualOptions& other);
    void DoROpt(string opt, string arg, int type);
    string getName(int pos);
    string getArg(string name);
    ~ActualOptions();
private:
    int defaultCapacity;
    int capacity;
    int size;
};

class NoMatchHint{
public:
    NoMatchHint();
    NoMatchHint& operator=(const NoMatchHint& other);
    ~NoMatchHint();
};

class ExactMistmach{
public:
    ExactMistmach();
    ExactMistmach& operator=(const  ExactMistmach& other);
    ~ExactMistmach();
};

class Variant{
    ActualOptions actualOptions;
    NoMatchHint noMatchHint;
    ExactMistmach exactMistmach;
    Rules rule;
public:
    friend class ActualOptions;
    friend class NoMatchHint;
    friend class ExactMistmach;
    friend class Rules;
    
    Variant();
    Variant& operator= (const Variant& other);
    Variant(Rules ThisRules);
    void ParseOption (vector<string> Args, int pos);
    int isOpt(string arg);
    int isDouble(string arg);
    int getArgCount(string arg);
    int getArgSecondCount(string arg);
    int getArgType(string arg);
    int isIntArg(string arg);
    string getHelpMessage(string arg);

    int getSizeROpt();
    string getRName(int pos);
    string getRArg(string name);

    ~Variant();
};

class Opt{
    string LongForm;
    string ShortForm;
    int argType;
    int argCount;
public:
    friend class Variant;
    friend class Rules;

    Opt();
    Opt(string LongForm, string ShortForm, int argCount, int argType);
    string getHint();
    string getType();
};

class Option{
    string name;
    string arg;
    int type;
    int count;
public:
    friend class Variant;
    friend class ActualOptions;

    Option();
    Option(string opt, string arg, int type);
    ~Option();
};
//--------------------------------Функции
//Общий парсер
Variant Parse(Rules ThisRules, int count, vector<string> Args);

#endif //GAMELIFE_PARSER_H
