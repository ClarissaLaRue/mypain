#ifndef CONTAINER_CONTAINERTRIT_H
#define CONTAINER_CONTAINERTRIT_H

#include <iostream>
#include <unordered_map>

const char magic_constant = char(0);

//константы для битовых операций
const char LAST = char (3);    //битовое представление 00000011
const char NOT_UNKNOWN = char(2);  //битовое представление 00000010
const char ALL_UNKNOWN = char(85);  //битовое представление 01010101
const char UNKNOWN1 = char(1);  //битовое представление 00000001
const char UNKNOWN2 = char(5);  //битовое представление 00000101
const char UNKNOWN3 = char(21); //битовое представление 00010101
const char CLEAR1 = char(252);  //битовое представление 11111100
const char CLEAR2 = char(240);  //битовое представление 11110000
const char CLEAR3 = char(192);  //битовое представление 11000000
const char FALSE1 = char(63); //битовое представление 00111111
const char FALSE2 = char(207); //битовое представление 11001111
const char FALSE3 = char(243); //битовое представление 11110011
const char FALSE4 = char(252); //битовое представление 11111100

enum class Trit {
    FALSE = -1,
    UNKNOWN = 0,
    TRUE = 1

};

class TritHolder;

class Iterator;

class TritSet {

public:
    TritSet();
    TritSet(int size);
    TritSet(const TritSet& other);

    void saveToStream(std::ostream &os);
    void loadFromStream(std::istream &ifs);

    TritSet& operator= (const TritSet& other);
    TritSet& operator& (const TritSet& other);
    TritSet& operator| (const TritSet& other);
    TritSet& operator~ ();
    TritHolder operator[](int index);

    void Set (Trit el, int index);
    char Get(int index);

    int capacity();
    int length();
    int cardinality(Trit el);
    std::unordered_map<int, int, std::hash<int>> cardinality();

    void shrink ();
    void trim (int lastIndex);

    virtual ~TritSet();

    Iterator begin();
    Iterator end();
private:
    friend class TritHolder;
    friend class Iterator;

    int FalseCount;
    int TrueCount;
    int quantityOfFalse();
    int quantityOfUnknown();
    int quantityOfTrue();
    void recalculateTrit(int lastIndex);

    void deleteSomeTrits(char* trits, int modIndex, int index);
    int getTrit (char trit, int parameter);
    char fromTrittoChar (Trit el);
    Trit fromChartoTrit (char ch);

    void resize(int size);
    int size;
    int realSize;
    int lastSetTrit;
    char* trits;
};

#endif //CONTAINER_CONTAINERTRIT_H