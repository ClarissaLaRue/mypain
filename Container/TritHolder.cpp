#include "TritSet.h"
#include "TritHolder.h"

//Конструктор
TritHolder::TritHolder(TritSet& TS, int index) : TS(TS) , index(index) {}

//Операторы
TritHolder& TritHolder::operator= (Trit el){
    this->TS.Set(el, index);
    return *this;
}

//Варианты когда подают просто трит или TritHolder
Trit TritHolder::operator& (const TritHolder& other){
    char ch1 = this->TS.Get(this->index);
    char ch2 = other.TS.Get(other.index);
    Trit res = doAnd(  TS.fromChartoTrit(ch1), TS.fromChartoTrit(ch2));
    return res;
}

Trit TritHolder::operator| (const TritHolder& other){
    char ch1 = this->TS.Get(this->index);
    char ch2 = other.TS.Get(other.index);
    Trit res = doOr( TS.fromChartoTrit(ch1), TS.fromChartoTrit(ch2));
    return res;
}

Trit TritHolder::operator& (Trit el){
    char ch = this->TS.Get(this->index);
    Trit res = doAnd( TS.fromChartoTrit(ch), el);
    return res;
}

Trit TritHolder::operator| (Trit el){
    char ch = this->TS.Get(this->index);
    Trit res = doOr( TS.fromChartoTrit(ch), el);
    return res;
}

Trit TritHolder::doAnd (Trit el1, Trit el2) {
    Trit res = Trit::UNKNOWN;
    if ((el1 == Trit::TRUE) && (el2 == Trit::TRUE)){
        res = Trit::TRUE;
    }
    if ((el1 == Trit::FALSE) || (el2 == Trit::FALSE)){
        res = Trit::FALSE;
    }
    return res;
}

Trit TritHolder::doOr (Trit el1, Trit el2) {
    Trit res = Trit::UNKNOWN;
    if ((el1 == Trit::TRUE) || (el2 == Trit::TRUE)){
        res = Trit::TRUE;
    }
    if ((el1 == Trit::FALSE) && (el2 == Trit::FALSE)){
        res = Trit::FALSE;
    }
    return res;
}

//Не реализуем по другому, потому что только для одного трита применяется
Trit TritHolder::operator~ (){
    Trit el = TS.fromChartoTrit(this->TS.Get(this->index));
    Trit res = Trit ::UNKNOWN;
    if (el == Trit ::TRUE){
        res = Trit ::FALSE;
    }
    if (el == Trit ::FALSE){
        res = Trit ::TRUE;
    }
    return res;
}



