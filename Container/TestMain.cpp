#include <gtest/gtest.h>
#include <iostream>
#include "TritSet.h"
#include "TritHolder.h"

//Проверка TritSet

//Вставка и Доставание элемента
TEST(TestForTritSet, StartTest) {
    TritSet A(5);
    A[0] = Trit::TRUE;
    char ch = char(3);
    EXPECT_TRUE(A.Get(0) == ch);
    A[2] = Trit::UNKNOWN;
    ch = char(1);
    EXPECT_TRUE(A.Get(2) == ch);
    A[3] = Trit::TRUE;
    ch = char(3);
    EXPECT_TRUE(A.Get(3) == ch);
    A[4] = Trit::FALSE;
    ch = char(0);
    EXPECT_TRUE(A.Get(4) == ch);
    //При обращении к элементу когда index>size возвращается Unknown;
    ch = char(1);
    EXPECT_TRUE(A.Get(10) == ch);
}

//Количество тритовб, количество тритов после удаления блока
TEST(TestForTritSet, Cardinality){
    TritSet A(20);
    A[0] = Trit ::TRUE;
    A[1] = Trit ::FALSE;
    A[2] = Trit ::FALSE;
    EXPECT_EQ(1, A.cardinality(Trit::TRUE));
    EXPECT_EQ(2, A.cardinality(Trit::FALSE));
    EXPECT_EQ(0, A.cardinality(Trit::UNKNOWN));
    A[3] = Trit ::UNKNOWN;
    A[4] = Trit ::FALSE;
    EXPECT_EQ(1, A.cardinality(Trit::UNKNOWN));
}

TEST(TestForTritSet, CardinalityAfterTrim){
    TritSet A(20);
    A[0] = Trit ::TRUE ;
    A[1] = Trit ::FALSE ;
    A[3] = Trit ::TRUE ;
    A[7] = Trit ::TRUE;
    A[14] = Trit ::FALSE ;
    A[18] = Trit ::FALSE ;
    A[19] = Trit ::TRUE ;
    A.trim(12);
    EXPECT_EQ(3, A.cardinality(Trit::TRUE));
    EXPECT_EQ(1, A.cardinality(Trit::FALSE));
    EXPECT_EQ(9, A.cardinality(Trit::UNKNOWN));
    A.trim(4);
    EXPECT_EQ(2, A.cardinality(Trit::TRUE));
    EXPECT_EQ(1, A.cardinality(Trit::FALSE));
    EXPECT_EQ(2, A.cardinality(Trit::UNKNOWN));
}

//Проверка памяти
TEST(TestForTritSet, Memory) {
    TritSet A(1000);
    int allocLength = A.capacity();
    ASSERT_GE(allocLength , 1000);
    A[10000] = Trit::UNKNOWN;
    ASSERT_EQ(allocLength , A.capacity());
    A[10000] = Trit::TRUE;
    ASSERT_LT(allocLength , A.capacity());
}

//Операторы Логики, изменение размера после операторов логики
TEST(TestForTritSet, TestForOperatorAND){
    TritSet A(9);
    A[0] = Trit ::TRUE;
    A[1] = Trit ::TRUE;
    A[2] = Trit ::TRUE;
    A[3] = Trit ::FALSE;
    A[4] = Trit ::FALSE;
    A[5] = Trit ::FALSE;
    TritSet B(9);
    B[0] = Trit ::TRUE;
    B[1] = Trit ::FALSE;
    B[3] = Trit ::TRUE;
    B[4] = Trit ::FALSE;
    B[6] = Trit ::TRUE;
    B[7] = Trit ::FALSE;
    A.operator&(B);
    EXPECT_EQ(3, A.Get(0));
    EXPECT_EQ(0, A.Get(1));
    EXPECT_EQ(1, A.Get(2));
    EXPECT_EQ(0, A.Get(3));
    EXPECT_EQ(0, A.Get(4));
    EXPECT_EQ(0, A.Get(5));
    EXPECT_EQ(1, A.Get(6));
    EXPECT_EQ(0, A.Get(7));
    EXPECT_EQ(1, A.Get(8));
}

TEST(TestForTritSet, TestForOperatorOR){
    TritSet A(9);
    A[0] = Trit ::TRUE;
    A[1] = Trit ::TRUE;
    A[2] = Trit ::TRUE;
    A[3] = Trit ::FALSE;
    A[4] = Trit ::FALSE;
    A[5] = Trit ::FALSE;
    TritSet B(9);
    B[0] = Trit ::TRUE;
    B[1] = Trit ::FALSE;
    B[3] = Trit ::TRUE;
    B[4] = Trit ::FALSE;
    B[6] = Trit ::TRUE;
    B[7] = Trit ::FALSE;
    A.operator|(B);
    EXPECT_EQ(3, A.Get(0));
    EXPECT_EQ(3, A.Get(1));
    EXPECT_EQ(3, A.Get(2));
    EXPECT_EQ(3, A.Get(3));
    EXPECT_EQ(0, A.Get(4));
    EXPECT_EQ(1, A.Get(5));
    EXPECT_EQ(3, A.Get(6));
    EXPECT_EQ(1, A.Get(7));
    EXPECT_EQ(1, A.Get(8));
}

TEST(TestForTritSet, TestForOperatorNOT){
    TritSet A(3);
    A[0] = Trit ::TRUE;
    A[1] = Trit ::FALSE;
    A.operator~();
    EXPECT_EQ(0, A.Get(0));
    EXPECT_EQ(3, A.Get(1));
    EXPECT_EQ(1, A.Get(2));
}

TEST(TestForTritSet, OverloadAND){
    TritSet A(1000);
    TritSet B(2000);
    A[100] = Trit ::TRUE;
    B[100] = Trit ::TRUE;
    A[20] = Trit ::FALSE;
    TritSet C(1000);
    C = (A & B);
    ASSERT_EQ(C.capacity() , B.capacity());
}

TEST(TestForTritSet, OverloadOR){
    TritSet A(1000);
    TritSet B(2000);
    A[100] = Trit ::TRUE;
    B[100] = Trit ::FALSE;
    A[20] = Trit ::FALSE;
    TritSet C(1000);
    C = (A | B);
    ASSERT_EQ(C.capacity() , B.capacity());
}

TEST(TestForTritHolder, Operators){
    TritSet A(9);
    A[1] = Trit ::TRUE;
    A[2] = Trit ::TRUE;
    A[3] = Trit ::FALSE;
    A[4] = Trit ::FALSE;
    TritSet B(9);
    B[1] = (A[1] & A[2]);
    B[2] = (A[2] | A[3]);
    B[3] = A[3] | Trit ::TRUE;
    EXPECT_EQ(3, B.Get(1));
    EXPECT_EQ(3, B.Get(2));
    EXPECT_EQ(3, B.Get(3));

}

//Оператор = и конструктор копирования
TEST(TestForTritSet, TestForOperatorEQU){
    TritSet A(3);
    A[0] = Trit ::TRUE;
    A[1] = Trit ::FALSE;
    TritSet B(2);
    B = A;
    EXPECT_EQ(3, B.capacity());
    EXPECT_EQ(3, B.Get(0));
    EXPECT_EQ(0, B.Get(1));
    EXPECT_EQ(1, B.Get(2));
}

//Длина, Начало, Конец
TEST(TestForTritSet, length){
    TritSet A(20);
    A[4] = Trit ::TRUE;
    EXPECT_EQ(5 , A.length());
    A[2] = Trit ::FALSE;
    EXPECT_EQ(5 , A.length());
    A[7] = Trit ::UNKNOWN;
    EXPECT_EQ(5 , A.length());
}

TEST(TestForTritSet, BeginAndEnd){
    TritSet A(10);
    A[5] = Trit ::TRUE;
    A[7] = Trit ::FALSE;
    EXPECT_EQ(0, A.begin().getIndex() );
    EXPECT_EQ(8, A.end().getIndex() );
}

//Удаление
TEST(TestForTritSet, Trim){
    TritSet A(9);
    EXPECT_EQ(9, A.capacity());
    A[3] = Trit ::TRUE;
    A[5] = Trit ::FALSE;
    A.trim(4);
    EXPECT_EQ(4, A.capacity());
    EXPECT_EQ(1, A.Get(5));
    EXPECT_EQ(1, A.Get(6));
}

TEST(TestForTritSet, Shrink){
    TritSet A(9);
    EXPECT_EQ(9, A.capacity());
    A[3] = Trit ::TRUE;
    A[5] = Trit ::FALSE;
    A.shrink();
    EXPECT_EQ(5, A.capacity());
}

int main(int argc, char** argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
