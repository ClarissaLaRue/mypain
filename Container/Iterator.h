#ifndef CONTAINER_ITERATOR_H
#define CONTAINER_ITERATOR_H
#include "TritSet.h"

class Iterator {
    TritSet& TS;
    int index;
public:
    //Конструктор
    Iterator(TritSet& TS, int index);
    //Операторы итератора
    Iterator& operator++();
    Iterator& operator++(int index);
    bool operator==(const Iterator& other);
    bool operator!=(const Iterator& other);
    int getIndex();
};


#endif //CONTAINER_ITERATOR_H
