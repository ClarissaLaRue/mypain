#ifndef CONTAINER_TRITHOLDER_H
#define CONTAINER_TRITHOLDER_H
#include "TritSet.h"
#include "Iterator.h"

const int TritAND = 1;
const int TritOR = 2;

class TritHolder{
    TritSet& TS;
    int index;
public:
    TritHolder(TritSet& TS, int index);
    TritHolder& operator= (Trit el);
    Trit operator& (const TritHolder& other);
    Trit operator| (const TritHolder& other);
    Trit operator& (Trit el);
    Trit operator| (Trit el);
    Trit operator~ ();
    Trit doAnd (Trit el1, Trit el2);
    Trit doOr (Trit el1, Trit el2);
};


#endif //CONTAINER_TRITHOLDER_H
