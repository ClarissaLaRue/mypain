#include "Iterator.h"

Iterator::Iterator(TritSet& TS, int index) : TS(TS), index(index) {};

Iterator& Iterator::operator++(){
    if (index + 1 < this->TS.size){
        index++;
    }
    return *this;
}

Iterator& Iterator::operator++(int index){
    auto* cell = new Iterator(this->TS, index + 1);
    return *cell;
}

bool Iterator::operator== (const Iterator& other){
    return ((this->TS.trits == other.TS.trits) && (this->index == other.index));
}

bool Iterator::operator!= (const Iterator& other){
    return ((this->TS.trits != other.TS.trits) && (this->index != other.index));
}

int Iterator::getIndex(){
    return index;
}


