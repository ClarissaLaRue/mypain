#include "TritSet.h"
#include "TritHolder.h"

//Конструкторы
    TritSet::TritSet() {
        size = 0;
        realSize = 0;
        FalseCount = 0;
        TrueCount = 0;
        lastSetTrit = -1;
    }

    TritSet::TritSet(int size){
        this->size=0;
        resize(size);
        for (int i = 0; i < (size/4 + 1); i++){
            trits[i] = ALL_UNKNOWN;
        }
        lastSetTrit = -1;
        FalseCount = 0;
        TrueCount = 0;
        realSize = 0;
    }

    TritSet::TritSet(const TritSet &other) {
        this->size = other.size;
        this->realSize = other.realSize;
        for (int i = 0; i < size; i++){
            this->trits[i] = other.trits[i];
        }
        this->lastSetTrit = other.lastSetTrit;
        this->FalseCount = other.FalseCount;
        this->TrueCount = other.TrueCount;
    }

//Деструктор
    TritSet::~TritSet() {
        delete[](trits);
    }

//Вывод на экран и запись с экрана
    void TritSet::saveToStream(std::ostream &os) {
        os << magic_constant << " " << size<< " ";
    for (int i=0; i<size/4 + 1; i++){
        os << trits[i];
    }
    }

    void TritSet::loadFromStream(std::istream &ifs){//До конца файла
        int i=0;
        char ch = char(0);
        int newSize = 0;
        ifs >> ch;
        if (ch == char(0)){
            ifs >> newSize;
            if (newSize > size){
                resize(newSize);
            }
            while (i < newSize){
                ifs >> trits[i];
                i++;
            }
        }

    }

//Получаем char из Trit и наоборот из Char в Trit
    char TritSet::fromTrittoChar(Trit el) {
        switch (el) {
            case Trit::FALSE:
                return char(0);  // FALSE заполняется как 00
            case Trit::UNKNOWN:
                return char(1);  // UNKNOWN заполняется как 01
            case Trit::TRUE:
                return char(3);  // TRUE заполняется как 11
        }
        return char(1);
    }

    Trit TritSet::fromChartoTrit (char ch){
        switch (ch) {
            case char(0):
                return Trit ::FALSE;
            case char(1):
                return Trit ::UNKNOWN;
            case char(3):
                return Trit ::TRUE;
        }
        return Trit ::UNKNOWN;
    }

//Операторы
    TritSet& TritSet::operator= (const TritSet &other) {
        if (this == &other)
            return *this;
        this->size = other.size;
        this->realSize = other.realSize;
        for (int i = 0; i < realSize; i++){
            this->trits[i] = other.trits[i];
        }
        this->lastSetTrit = other.lastSetTrit;
        this->FalseCount = other.FalseCount;
        this->TrueCount = other.TrueCount;
        return *this;
    }

    TritHolder TritSet::operator[](int index){
        int count = index;
        TritHolder* cellPtr = new TritHolder(*this, count);
        return *cellPtr;
    }

    TritSet& TritSet::operator& (const TritSet& other){
        if(this->size < other.size){
            resize(other.size);
        }
        int max = this->realSize < other.realSize ? (other.realSize / 4)+1 : (this->realSize / 4)+1;
        int min = this->realSize > other.realSize ? (other.realSize / 4)+1 : (this->realSize / 4)+1;
        //Делаем & для общих элементов
        for (int i=0; i<min; i++){
            this->trits[i] &= other.trits[i];
        }
        //Добавляем остальные триты из большего
        if (this->realSize < other.realSize ){
            this->realSize = other.realSize;
            for (int i=min; i<max; i++){
                this->trits[i] = other.trits[i];
            }
        }
        return *this;
    }

    TritSet& TritSet::operator| (const TritSet& other){
        if(this->size < other.size){
            resize(other.size);
        }
        int max = this->realSize < other.realSize ? (other.realSize / 4)+1 : (this->realSize / 4)+1;
        int min = this->realSize > other.realSize ? (other.realSize / 4)+1 : (this->realSize / 4)+1;
        //Делаем | для общих элементов
        for (int i=0; i<min; i++){
            this->trits[i] |= other.trits[i];
        }
        //Добавляем остальные триты из большего
        if (this->realSize < other.realSize ){
            this->realSize = other.realSize;
            for (int i=min; i<max; i++){
                this->trits[i] = other.trits[i];
            }
        }
        return *this;
}

    TritSet& TritSet::operator~ (){
        for (int i = 0; i < realSize; i += 4){
            for(int j = 1; j <= 4; j++){
                if (getTrit(trits[i], j) == 1){
                    //Если у нас трит Unknown, то представляем его в виде 10, что бы при битовом не, он снова стал сам собой
                    trits[i] &= ~(UNKNOWN1 << ((4 - j) * 2));
                    trits[i] |= (NOT_UNKNOWN << ((4 - j) * 2));
                }
            }
        }
        for (int i = 0; i < (realSize / 4) + 1 ; i++){
            trits[i] = ~trits[i];
        }
        return *this;
    }

//Вставка нового трита в массив Char
    void TritSet::Set(Trit el, int index) {
        if (index > size) {
            if (el == Trit::UNKNOWN)
                return;
            resize(index+1);
        }
        if (el == Trit::UNKNOWN)
            return;
        char ch = fromTrittoChar(el);
        if (ch == 0){
            FalseCount++;
            if (index % 4 == 0){
                trits[index / 4] &= FALSE1;
            }
            if (index % 4 == 1){
                trits[index / 4] &= FALSE2;
            }
            if (index % 4 == 2){
                trits[index / 4] &= FALSE3;
            }
            if (index % 4 == 3){
                trits[index / 4] &= FALSE4;
            }
        }
        if (ch == 3){
            TrueCount++;
            trits[index / 4] |= (ch << (3 - index % 4) * 2);
        }
        lastSetTrit = index;
        if ((ch != 1) && (realSize < index)){
            realSize = index;
        }
    }

//Достаем трит
    char TritSet::Get(int index){
        if (index > size) {
            return char(1);
        }
        char ch = trits[index / 4];
        ch = ((ch >> ((3 - index % 4) * 2)) & LAST);
        return ch;
}

//берем parametr(первый, второй и тд) трит из char
    int TritSet::getTrit(char trit, int parameter) {
        //делаем сдвиг так, что б нужный нам трит стоял на последней позиции, а потом делаем битовое И с 00000011
        trit = ((trit >> ((4 - parameter) * 2)) & LAST);
        return static_cast <int> (trit);
    }

//Изменение размера
    void TritSet::resize(int size){
    int oldSize = this->size;
    int newSize = size;
    char *buf = trits;
    trits = new char[newSize];
    for(int i = 0; i < (newSize / 4 + 1); i++){
        if(i < oldSize)
            trits[i] = buf[i];
        else
            trits[i] = ALL_UNKNOWN;
    }
    this->size = size;
    delete[](buf);
}

//Вместимость
    int TritSet::capacity() {
        return size;
    }

//Длина
    int TritSet::length(){
        return realSize + 1;
};

//Начало и конец
    Iterator TritSet::begin(){
        Iterator* cell = new Iterator(*this, 0);
        return *cell;
    }

    Iterator TritSet::end(){
        Iterator* cell = new Iterator(*this, this->length());
        return *cell;
    }

//Количество тритов
    int TritSet::quantityOfFalse() {
        return FalseCount;
    }

    int TritSet::quantityOfTrue() {
        return TrueCount;
    }

    int TritSet::quantityOfUnknown() {
    return realSize + 1 - quantityOfFalse() - quantityOfTrue();
}

    int TritSet::cardinality(Trit el){
        switch(el){
            case Trit::TRUE:
                return this->quantityOfTrue();
            case Trit::FALSE:
                return this->quantityOfFalse();
            case Trit::UNKNOWN:
                return this->quantityOfUnknown();
        }
        return this->quantityOfUnknown();
    }

    std::unordered_map<int, int, std::hash<int>> TritSet::cardinality(){
    std::unordered_map<int, int, std::hash<int>> map({
                        {static_cast<int>(Trit::TRUE), this->cardinality(Trit::TRUE)},
                        {static_cast<int>(Trit::FALSE), this->cardinality(Trit::FALSE)},
                        {static_cast<int>(Trit::UNKNOWN), this->cardinality(Trit::UNKNOWN)}
                });
    return map;
}

//Пересчет тритов при удалении
    void TritSet::recalculateTrit(int lastIndex){
        if (lastIndex > size-lastIndex){
            for (int i = lastIndex + 1; i<size; i++){
                if (Get(i) == 0){
                    FalseCount--;
                }
                if (Get(i) == 3){
                    TrueCount--;
                }
            }

        }else {
            FalseCount=0;
            TrueCount=0;
            for (int i = 0; i<lastIndex; i++){
                if (Get(i) == 0){
                    FalseCount++;
                }
                if (Get(i) == 3){
                    TrueCount++;
                }
            }
        }
    }

//Удаляем modindex трит в Char (то есть делаем их Unknown)
//modindex <=3
    void TritSet::deleteSomeTrits(char *trits, int modIndex, int index) {
        int i = index - 4 + modIndex;
        if (modIndex == 1) {
            trits[i] = ((trits[i] & CLEAR3) | UNKNOWN3);
        }
        if (modIndex == 2) {
            trits[i] = ((trits[i] & CLEAR2) | UNKNOWN2);
        }
        if (modIndex == 3) {
            trits[i] = ((trits[i] & CLEAR1) | UNKNOWN1);
        }
    }

//Освобождение памяти до начала или до последнего установленного трита
    void TritSet::shrink (){
        trim(lastSetTrit); //Удаляем триты после последнего установленного
}

//Удаление тритов с lastIndex и до конца(ставим везде значение Unknown)
    void TritSet::trim(int lastIndex) {
        int modIndex = lastIndex % 4;
        this->recalculateTrit(lastIndex);
        if (modIndex != 0) {
            deleteSomeTrits(trits, modIndex, lastIndex);
        }
        for(int i=(lastIndex / 4 +1); i < (size / 4); i++){
            trits[i] = ALL_UNKNOWN;
        }
        this->realSize = lastIndex;
    }
